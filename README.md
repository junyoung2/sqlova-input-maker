### What it does
- Takes in two files (one has WikiSQL represented as json e.g. train.jsonl in data, one has NL sentences corrosponding to SQLs) and produces .jsonl file that sqlova can use for training

### Preliminaries
- Download Stanford CoreNLP server (Download link)[] and unzip (location does not matter) then run `sh usage.txt`
- Make and activate conda environment using `clove_env.yml` in `environments` directory in sqlova directory

### How to run it
- Make directory called `aug_data`
- In the aug_data directory, make a directory with the dataset name you want to generate (we'll call it DATASET_DIR)
- In DATASET_DIR, put in text file containing lines of form `NUMBER\tNL` where NUMBER is line number of SQL that corrosponds to NL (we'll call text file `nl.txt`)
- Run `python make_aug_train_data.py -input=aug_data/DATASET_DIR/nl.txt` -sql_file=(PATH_TO_SQL_FILE)
    - Ex) `python make_aug_train_data.py -input=aug_data/copynet/nl.txt -sql_file=data/sampled_sql.jsonl`
- Upon completion, in DATASET_DIR there will be
    - `tokenized.txt`, the tokenized version of NL sentences you gave as input (can be used to calculate BLEU, SELF-BLEU...)
    - `train_tok.jsonl`, .jsonl file used to train sqlova, it is also appended with the original WikiSQL training set

### Other options
- Giving the `-no_append` option will give `train_tok.jsonl` made with ONLY the NL sentences you gave as input
