import json
 
MAX_LEN = 12
 
def resolve_redundant(nodes, r_conds):
    n = len(r_conds)
    for i in range(n):
        if r_conds[i][1] == None:
            continue
        for j in range(1, n - 1 - i):
            if r_conds[i][1] == r_conds[i+j][1]:
                r_conds[i+j][1] = None
                nodes[r_conds[i+j][0]-1][3].remove(r_conds[i+j][0])
                nodes[r_conds[i+j][0]-1][3].append(r_conds[i][0])
 
    r_dicts = dict(r_conds)
    result = []
    for item in nodes:
        if item[1] not in r_dicts or r_dicts[item[1]] != None:
            result.append(item)
 
    return result
 
def add_empty_nodes(nodes):
    cur_len = len(nodes)
    ids = dict([[i, 0] for i in range(MAX_LEN)])
 
    for n in nodes:
        ids[n[1]] = 1
 
    for key in ids:
        if ids[key] == 0:
            e_node = (str(key), key, "<EMP>", [])
            nodes.append(e_node)
 
    return nodes
 
def sql_to_graph(sql, schema, nl):
    agg_ops = ['', 'MAX', 'MIN', 'COUNT', 'SUM', 'AVG']
    cond_ops = ['=', '>', '<', 'OP']
 
    graph = {"seq": None,
             "g_ids": {},
             "g_ids_features": {},
             "g_adj": {}}

    if sql["agg"] != 0:
        rep = "select {agg} {sel}".format(agg=agg_ops[sql["agg"]], sel=schema[sql["sel"]])
    else:
        rep = "select {sel}".format(sel=schema[sql["sel"]])

    rep += ' where ' + ' and '.join(['{} {} {}'.format(schema[cond[0]], cond_ops[cond[1]], cond[2]) for cond in sql["conds"]])

    return rep.lower()

    """
 
    # seq is for nl result of the decoder
    #graph["seq"] = " ".join(nl)
    graph["seq"] = nl
 
    idx = 0
 
    # First add SELECT node
    nodes = [(str(idx), idx, "SELECT", [1])]
    idx += 1
 
    # Then add projection column
    p_col_node = (str(idx), idx, schema[sql["sel"]], [])
    nodes.append(p_col_node)
    idx += 1
 
    # Add aggregation node if exists
    if sql["agg"] != 0:
        is_agg = True
        nodes.append((str(idx), idx, agg_ops[sql["agg"]], []))
        p_col_node[3].append(idx)
        idx += 1
 
        # Add logical operator node, which is AND in WikiSQL
    a_node = (str(idx), idx, "AND", [0])
    nodes.append(a_node)
    idx += 1
 
    r_conds = []
 
    # For each condition value, make nodes for it
    for cond in sql["conds"]:
        cond_col = schema[cond[0]]
        cond_op = cond_ops[cond[1]]
        cond_val = cond[2]
        if isinstance(cond_val, int) or isinstance(cond_val, float):
            cond_val = str(cond_val)
        else:
            cond_val = u'"' + cond_val + u'"'
 
        nodes.append((str(idx), idx, cond_col, [idx+1]))
        a_node[3].append(idx)
        idx += 1
 
        nodes.append((str(idx), idx, cond_op + cond_val, []))
        r_conds.append([idx, cond_op + cond_val])
        idx += 1
 
    if max_node < len(nodes):
        max_node = len(nodes)
 
    nodes = resolve_redundant(nodes, r_conds)
    nodes = add_empty_nodes(nodes)
 
    assert len(nodes) == MAX_LEN
 
    for n in nodes:
        graph["g_ids"][n[0]] = n[1]
        graph["g_ids_features"][n[0]] = n[2]
        graph["g_adj"][n[0]] = n[3]
 
    return graph, max_node
    """
 
datasets = ['train', 'test', 'dev']

rid = 0

for d in datasets:
    table_file = open(d + ".tables.jsonl")
    data_file = open(d + ".jsonl")
    #write_file = open(d + ".data", "w")
    write_file = open("copynet_" + d + ".txt", "w", encoding='utf-8')
 
    tbls = {}

     
    for line in table_file:
        jl = json.loads(line)
        tbls[jl["id"]] = jl["header"]
 
    for line in data_file:
        jl = json.loads(line)
        rep = sql_to_graph(jl['sql'], tbls[jl['table_id']], jl['question'])
        line = jl['question'].lower().replace("\t", "") + "\t" + rep.replace("\t", "")
        write_file.write(line)
        write_file.write('\n')
        rid += 1
        """
        json.dump(graph, write_file)
        write_file.write('\n')
        """
 
    #print(d + ": " + str(max_node))
