#!/usr/bin/env python3
# docker run --name corenlp -d -p 9000:9000 vzhong/corenlp-server
# Wonseok Hwang. Jan 6 2019, Comment added
from argparse import ArgumentDefaultsHelpFormatter, ArgumentParser
import os
import records
import ujson as json
from stanza.nlp.corenlp import CoreNLPClient
from tqdm import tqdm
import copy
from wikisql.lib.common import count_lines, detokenize
from wikisql.lib.query import Query

from rule_based_sql2nl.generate import parse_sql, sql_to_nl
from rule_based_sql2nl.ppdb import get_ppdb_phrases
from rule_based_sql2nl.utils import get_number_of_dict_items
from rule_based_sql2nl.templates import jy, nsp, jy_nsp, faulty_nsp_10


client = None


def annotate(sentence, lower=True):
    global client
    if client is None:
        client = CoreNLPClient(default_annotators='ssplit,tokenize'.split(','))
    words, gloss, after = [], [], []
    for s in client.annotate(sentence):
        for t in s:
            words.append(t.word)
            gloss.append(t.originalText)
            after.append(t.after)
    if lower:
        words = [w.lower() for w in words]
    return {
        'gloss': gloss,
        'words': words,
        'after': after,
        }


def annotate_example(example, table):
    ann = {'table_id': example['table_id']}
    ann['question'] = annotate(example['question'])
    ann['table'] = {
        'header': [annotate(h) for h in table['header']],
    }
    ann['query'] = sql = copy.deepcopy(example['sql'])
    for c in ann['query']['conds']:
        c[-1] = annotate(str(c[-1]))

    q1 = 'SYMSELECT SYMAGG {} SYMCOL {}'.format(Query.agg_ops[sql['agg']], table['header'][sql['sel']])
    q2 = ['SYMCOL {} SYMOP {} SYMCOND {}'.format(table['header'][col], Query.cond_ops[op], detokenize(cond)) for col, op, cond in sql['conds']]
    if q2:
        q2 = 'SYMWHERE ' + ' SYMAND '.join(q2) + ' SYMEND'
    else:
        q2 = 'SYMEND'
    inp = 'SYMSYMS {syms} SYMAGGOPS {aggops} SYMCONDOPS {condops} SYMTABLE {table} SYMQUESTION {question} SYMEND'.format(
        syms=' '.join(['SYM' + s for s in Query.syms]),
        table=' '.join(['SYMCOL ' + s for s in table['header']]),
        question=example['question'],
        aggops=' '.join([s for s in Query.agg_ops]),
        condops=' '.join([s for s in Query.cond_ops]),
    )
    ann['seq_input'] = annotate(inp)
    out = '{q1} {q2}'.format(q1=q1, q2=q2) if q2 else q1
    ann['seq_output'] = annotate(out)
    ann['where_output'] = annotate(q2)
    assert 'symend' in ann['seq_output']['words']
    assert 'symend' in ann['where_output']['words']
    return ann

def find_sub_list(sl, l):
    # from stack overflow.
    results = []
    sll = len(sl)
    for ind in (i for i, e in enumerate(l) if e == sl[0]):
        if l[ind:ind + sll] == sl:
            results.append((ind, ind + sll - 1))

    return results

def check_wv_tok_in_nlu_tok(wv_tok1, nlu_t1):
    """
    Jan.2019: Wonseok
    Generate SQuAD style start and end index of wv in nlu. Index is for of after WordPiece tokenization.

    Assumption: where_str always presents in the nlu.

    return:
    st_idx of where-value string token in nlu under CoreNLP tokenization scheme.
    """
    g_wvi1_corenlp = []
    nlu_t1_low = [tok.lower() for tok in nlu_t1]
    for i_wn, wv_tok11 in enumerate(wv_tok1):
        wv_tok11_low = [tok.lower() for tok in wv_tok11]
        results = find_sub_list(wv_tok11_low, nlu_t1_low)
        st_idx, ed_idx = results[0]

        g_wvi1_corenlp.append( [st_idx, ed_idx] )

    return g_wvi1_corenlp


def annotate_example_ws(example, table):
    """
    Jan. 2019: Wonseok
    Annotate only the information that will be used in our model.
    """
    #ann = {'table_id': example['table_id'],'phase': example['phase']}
    ann = {'table_id': example['table_id']}
    _nlu_ann = annotate(example['question'])
    ann['question'] = example['question']
    ann['question_tok'] = _nlu_ann['gloss']
    # ann['table'] = {
    #     'header': [annotate(h) for h in table['header']],
    # }
    ann['sql'] = example['sql']
    ann['query'] = sql = copy.deepcopy(example['sql'])

    conds1 = ann['sql']['conds']
    wv_ann1 = []
    for conds11 in conds1:
        _wv_ann1 = annotate(str(conds11[2]))
        wv_ann11 = _wv_ann1['gloss']
        wv_ann1.append( wv_ann11 )

        # Check whether wv_ann exsits inside question_tok

    try:
        wvi1_corenlp = check_wv_tok_in_nlu_tok(wv_ann1, ann['question_tok'])
        ann['wvi_corenlp'] = wvi1_corenlp
    except:
        ann['wvi_corenlp'] = None
        ann['tok_error'] = 'SQuAD style st, ed are not found under CoreNLP.'

    return ann


def is_valid_example(e):
    if not all([h['words'] for h in e['table']['header']]):
        return False
    headers = [detokenize(h).lower() for h in e['table']['header']]
    if len(headers) != len(set(headers)):
        return False
    input_vocab = set(e['seq_input']['words'])
    for w in e['seq_output']['words']:
        if w not in input_vocab:
            print('query word "{}" is not in input vocabulary.\n{}'.format(w, e['seq_input']['words']))
            return False
    input_vocab = set(e['question']['words'])
    for col, op, cond in e['query']['conds']:
        for w in cond['words']:
            if w not in input_vocab:
                print('cond word "{}" is not in input vocabulary.\n{}'.format(w, e['question']['words']))
                return False
    return True


if __name__ == '__main__':
    parser = ArgumentParser(formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-input', type=str, default='data/train.in')
    parser.add_argument('-output', type=str)
    parser.add_argument('-lexical', action='store_true')
    parser.add_argument('-style', type=str)
    parser.add_argument('-phrase_style', type=str)
    parser.add_argument('-phrase_fraction')
    parser.add_argument('-choice', type=str)

    args = parser.parse_args()

    input_file = open(args.input, 'r', encoding='utf-8')

    lexical = args.lexical

    style_dict = globals()[args.style]
    template_list = style_dict['templates']

    phrase_dict = style_dict['phrases']

    if args.phrase_style:
        if args.phrase_style == 'ppdb':
            phrase_dict = get_ppdb_phrases(int(args.phrase_fraction))
        else:
            phrase_dict = globals()[args.phrase_style]['phrases']

    print("Number of phrases")
    print(get_number_of_dict_items(phrase_dict))

    if args.choice:
        choice = args.choice.split(',')
        tmp = []
        for i in choice:
            tmp.append(template_list[int(i)])
        template_list = tmp
    else:
        print("Using all templates")
    print("Number of templates")
    print(len(template_list))

    out_dir = os.path.join('results', args.output)

    if not os.path.isdir(out_dir):
        os.mkdir(out_dir)


    args_file = open(os.path.join(out_dir, 'args.txt'), 'w')

    for key, value in vars(args).items():
        args_file.write(f'{key}:{value}\n')
    
    args_file.close()

    nls = []

    nl_out_file = open(os.path.join(out_dir, 'nl.txt'), 'w')

    for row in input_file:
        parsed_sql = parse_sql(row.strip())
        nl = sql_to_nl(parsed_sql, template_list, phrase_dict, lexical)
        nls.append(nl)
        nl_out_file.write(nl + '\n')

    nl_out_file.close()

    sqls = []
    for s in open(os.path.join('data', 'train.jsonl')):
        sqls.append(json.loads(s.strip()))

    assert(len(sqls) == len(nls))

    jsons = []
    for i, s in enumerate(sqls):
        s['question'] = nls[i]
        jsons.append(s)

    answer_toy = not True
    toy_size = 10

    ftable = os.path.join('data/wikisql', 'train') + '.tables.jsonl'
    fout = os.path.join(out_dir, 'train_tok.jsonl')
    fo = open(fout, 'wt')

    tokenized_nl_file = open(os.path.join(out_dir, 'tokenized.txt'), 'w')

    print('loading tables')
    tables = {}
    for line in tqdm(open(ftable), total=count_lines(ftable)):
        d = json.loads(line)
        tables[d['id']] = d
    print('loading examples')


    fo = open(fout, 'wt')
    n_written = 0
    cnt = -1
    for j in tqdm(jsons, total=len(jsons)):
        cnt += 1
        d = j
        # a = annotate_example(d, tables[d['table_id']])
        a = annotate_example_ws(d, tables[d['table_id']])
        fo.write(json.dumps(a) + '\n')
        tokenized_nl_file.write(' '.join(a['question_tok']) + '\n')
        n_written += 1

        if answer_toy:
            if cnt > toy_size:
                break
    print('wrote {} examples'.format(n_written))
