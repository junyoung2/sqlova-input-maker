import random
import re
import inflect


random.seed(100)
p = inflect.engine()

def get_agg_phrase(agg, div, phrase_dict):
    assert(agg.lower() in phrase_dict)
    if div:
        return random.choice(phrase_dict[agg.lower()])
    else:
        return phrase_dict[agg.lower()][0]

def get_op_phrase(op, div, phrase_dict):
    assert(op in phrase_dict)
    op_word = ''
    if div:
        op_word = random.choice(phrase_dict[op])
        if op == '>' or op == '<':
            op_word += ' than'
    else:
        op_word = phrase_dict[op][0]
    return op_word

def get_select_phrase(div, phrase_dict):
    if div:
        return random.choice(phrase_dict['select'])
    else:
        return phrase_dict['select'][0]

def get_template(agg, conds, template_list):
    while True:
        found = True
        template = random.choice(template_list)
        if 'compatible' in template:
            if agg not in template['compatible']:
                found = False
        if 'incompatible' in template:
            for c in conds:
                if c[1] in template['incompatible']:
                    found = False
            if agg in template['incompatible']:
                found = False
        if found:
            return template


def fill_template(sel_col, agg, conds, template, lexical, phrase_dict):
    select_clause = template['select_clause']

    if agg != None:
        agg_phrase = get_agg_phrase(agg, lexical, phrase_dict)
        select_clause = select_clause.replace("{AGG}", agg_phrase)
        if agg == 'count':
            sel_col = convert_plural(sel_col)
    select_phrase = get_select_phrase(lexical, phrase_dict)
    select_clause = select_clause.replace("{SELECT}", select_phrase)
    select_clause = select_clause.replace("{SEL_COL}", sel_col)

    if not conds:
        return prep_for_return(select_clause)

    where_clauses = []
    for c in conds:
        where_clause = template['where_clause']
        op_phrase = get_op_phrase(c[1], lexical, phrase_dict)
        where_clause = where_clause.replace("{WHERE_COL}", c[0]).replace("{VAL}", c[2])
        where_clause = where_clause.replace("{OP}", op_phrase)
        where_clauses.append(where_clause)
    joined_where_clause = ' and '.join(where_clauses)
    ret = template['whole_template'].replace("{SELECT_C}", select_clause).replace("{WHERE_C}", joined_where_clause)
    return prep_for_return(ret)

def prep_for_return(ret):
    ret = ret.replace("{AGG}", '')


    if ret.startswith('What is') or ret.startswith("What's"):
        ret += ' ?'
    else:
        ret += ' .'
    ret = re.sub(' +', ' ', ret)
    return ret
    

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def get_number_of_dict_items(dict):
    ret = 0
    for key, item in dict.items():
        ret += len(item)
    return ret

def convert_plural(sel_col):
    sel_col_words = sel_col.split()
    last_word = sel_col_words[-1]
    if is_number(last_word) is False:
        if p.singular_noun(last_word) is False:
            if len(last_word) > 2 and last_word[-1].isalpha():
                sel_col_words[-1] = p.plural(last_word)
                return ' '.join(sel_col_words)
    return sel_col
