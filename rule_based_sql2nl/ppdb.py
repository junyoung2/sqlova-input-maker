import os

def get_ppdb_phrases(fraction):
    ret = {}
    for type in ['avg', 'count', 'equal', 'greater', 'less', 'max', 'min', 'select', 'sum']:
        if type is 'equal':
            dict_key = '='
        elif type is 'greater':
            dict_key = '>'
        elif type is 'less':
            dict_key = '<'
        else:
            dict_key = type
        ret[dict_key] = []
        if type is 'equal':
            ret[dict_key].append('')
        for line in open(os.path.join('rule_based_sql2nl', 'phrases', type + '.txt')):
            line = line.strip()
            if len(line) != 0:
                ret[dict_key].append(line)

    if fraction:
        for key in ret.keys():
            new_list_len = int(len(ret[key]) / fraction)
            if new_list_len == 0:
                new_list_len = 1
            ret[key] = ret[key][:new_list_len]
    return ret