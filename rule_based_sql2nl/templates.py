#Junyoung Kim generated templates

jy = {
    'templates': [
        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{WHERE_COL} is {OP} {VAL}',
         'whole_template': '{SELECT_C} where {WHERE_C}'},

        {'select_clause': 'How many {SEL_COL}',
         'where_clause': '{WHERE_COL} is {OP} {VAL}',
         'whole_template': '{SELECT_C} where {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg'],
         'compatible': ['count']},

        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{WHERE_COL} is {OP} {VAL}',
         'whole_template': 'When {WHERE_C} , {SELECT_C}'},

        {'select_clause': 'Which {SEL_COL}',
         'where_clause': '{VAL} as {WHERE_COL}',
         'whole_template': '{SELECT_C} has {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg', 'count', '<', '>']},

        {'select_clause': 'How many {SEL_COL}',
         'where_clause': '{VAL} as {WHERE_COL}',
         'whole_template': '{SELECT_C} has {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg', '<', '>'],
         'compatible': ['count']},

        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{WHERE_COL} being {OP} {VAL}',
         'whole_template': '{SELECT_C} with {WHERE_C}'}
    ],

    'phrases': {
        'select': ['What is', 'Tell me', "What's", 'Name', 'Give me', 'Show', 'Display', 'List'],
        'max': ['maximum', 'biggest', 'largest'],
        'min': ['minimum', 'smallest'],
        'sum': ['total'],
        'avg': ['average'],
        'count': ['number of'],
        '=': ['', 'equal to'],
        '>': ['greater', 'bigger', 'larger'],
        '<': ['less', 'smaller']
    }
}


nsp = {
    'templates': [
        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{WHERE_COL} is {OP} {VAL}',
         'whole_template': '{SELECT_C} where {WHERE_C}'},

        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{VAL}',
         'whole_template': '{SELECT_C} of {WHERE_C}',
         'incompatible': ['count', '<', '>']},

        {'select_clause': '{SELECT} all {SEL_COL}',
         'where_clause': '{WHERE_COL} {VAL}',
         'whole_template': '{SELECT_C} having {WHERE_C}',
         'incompatible': ['count', 'max', 'min', 'sum', 'avg', '<', '>']},

        {'select_clause': '{SEL_COL}',
         'where_clause': '{VAL}',
         'whole_template': '{WHERE_C} {SELECT_C}',
         'incompatible': ['count', 'max', 'min', 'sum', 'avg', '<', '>']},

        {'select_clause': 'How many {SEL_COL} are there',
         'where_clause': '{VAL}',
         'whole_template': '{SELECT_C} in {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg', '<', '>'],
         'compatible': ['count']}
    ],

    'phrases': {
        'select': ['What is'],
        'max': ['largest', 'biggest'],
        'min': ['smallest', 'lowest'],
        'sum': ['total', 'sum'],
        'avg': ['average'],
        'count': ['total number'],
        '=': ['', 'equal to'],
        '>': ['greater', 'bigger', 'larger'],
        '<': ['less', 'smaller']
    }
}

jy_nsp = {
    'templates': [
        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{WHERE_COL} is {OP} {VAL}',
         'whole_template': '{SELECT_C} where {WHERE_C}'},

        {'select_clause': 'How many {SEL_COL}',
         'where_clause': '{WHERE_COL} is {OP} {VAL}',
         'whole_template': '{SELECT_C} where {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg'],
         'compatible': ['count']},

        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{WHERE_COL} is {OP} {VAL}',
         'whole_template': 'When {WHERE_C} , {SELECT_C}'},

        {'select_clause': 'Which {SEL_COL}',
         'where_clause': '{VAL} as {WHERE_COL}',
         'whole_template': '{SELECT_C} has {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg', 'count', '<', '>']},

        {'select_clause': 'How many {SEL_COL}',
         'where_clause': '{VAL} as {WHERE_COL}',
         'whole_template': '{SELECT_C} has {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg', '<', '>'],
         'compatible': ['count']},

        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{WHERE_COL} being {OP} {VAL}',
         'whole_template': '{SELECT_C} with {WHERE_C}'},

        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{VAL}',
         'whole_template': '{SELECT_C} of {WHERE_C}',
         'incompatible': ['count', '<', '>']},

        {'select_clause': '{SELECT} all {SEL_COL}',
         'where_clause': '{WHERE_COL} {VAL}',
         'whole_template': '{SELECT_C} having {WHERE_C}',
         'incompatible': ['count', 'max', 'min', 'sum', 'avg', '<', '>']},

        {'select_clause': '{SEL_COL}',
         'where_clause': '{VAL}',
         'whole_template': '{WHERE_C} {SELECT_C}',
         'incompatible': ['count', 'max', 'min', 'sum', 'avg', '<', '>']},

        {'select_clause': 'How many {SEL_COL} are there',
         'where_clause': '{VAL}',
         'whole_template': '{SELECT_C} in {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg', '<', '>'],
         'compatible': ['count']}
    ],

    'phrases': {
        'select': ['What is', 'Tell me', "What's", 'Name', 'Give me', 'Show', 'Display', 'List'],
        'max': ['maximum', 'biggest', 'largest'],
        'min': ['minimum', 'smallest'],
        'sum': ['total'],
        'avg': ['average'],
        'count': ['number of'],
        '=': ['', 'equal to'],
        '>': ['greater', 'bigger', 'larger'],
        '<': ['less', 'smaller']
    }
}


faulty_nsp_10 = {
    'templates': [
        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '',
         'whole_template': '{SELECT_C}',
         'incompatible': ['count', 'sum', 'avg', '=', '<', '>']},

        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{WHERE_COL} is {OP} {VAL}',
         'whole_template': '{SELECT_C} where {WHERE_C}'},

        {'select_clause': '{SELECT} the {AGG} {SEL_COL}',
         'where_clause': '{VAL}',
         'whole_template': '{SELECT_C} of {WHERE_C}',
         'incompatible': ['count', '<', '>']},

        {'select_clause': '{SELECT} the {AGG} of {SEL_COL}',
         'where_clause': '',
         'whole_template': '{SELECT_C}',
         'incompatible': ['=', '<', '>']},

        {'select_clause': 'How many {SEL_COL} are there',
         'where_clause': '',
         'whole_template': '{SELECT_C}',
         'incompatible': ['max', 'min', 'sum', 'avg', '=', '<', '>'],
         'compatible': ['count']},

        {'select_clause': '{SELECT} the combined {SEL_COL}',
         'where_clause': '',
         'whole_template': '{SELECT_C}',
         'incompatible': ['max', 'min', 'count', 'avg', '=', '<', '>'],
         'compatible': ['sum']},

        {'select_clause': '{SELECT} all {SEL_COL}',
         'where_clause': '{WHERE_COL} {VAL}',
         'whole_template': '{SELECT_C} having {WHERE_C}',
         'incompatible': ['count', 'max', 'min', 'sum', 'avg', '<', '>']},

        {'select_clause': '{SEL_COL}',
         'where_clause': '{VAL}',
         'whole_template': '{WHERE_C} {SELECT_C}',
         'incompatible': ['count', 'max', 'min', 'sum', 'avg', '<', '>']},

        {'select_clause': '{SELECT} the {SEL_COL}',
         'where_clause': '{VAL}',
         'whole_template': '{SELECT_C} of {WHERE_C}',
         'incompatible': ['count', 'max', 'min', 'sum', 'avg', '<', '>']},

        {'select_clause': 'How many {SEL_COL} are there',
         'where_clause': '{VAL}',
         'whole_template': '{SELECT_C} in {WHERE_C}',
         'incompatible': ['max', 'min', 'sum', 'avg', '<', '>'],
         'compatible': ['count']}
    ],

    'phrases': {
        'select': ['What is'],
        'max': ['largest', 'biggest'],
        'min': ['smallest', 'lowest'],
        'sum': ['total', 'sum'],
        'avg': ['average'],
        'count': ['total number'],
        '=': ['', 'equal to'],
        '>': ['greater', 'bigger', 'larger'],
        '<': ['less', 'smaller']
    }
}

