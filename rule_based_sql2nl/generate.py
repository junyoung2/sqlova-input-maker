import sys
import nltk
from nltk.tokenize import word_tokenize
from nltk.tag import pos_tag
import spacy
from spacy import displacy
from collections import Counter
import argparse

from rule_based_sql2nl.utils import get_agg_phrase, get_op_phrase, get_select_phrase
from rule_based_sql2nl.utils import get_template, fill_template
from rule_based_sql2nl.utils import convert_plural, is_number

from rule_based_sql2nl.templates import jy, nsp

debug = False

def parse_sql(sql):
    words = sql.split()
    #print(words)
    length = len(words)

    i = 0

    assert(words[i].lower() == 'symselect')

    i = i + 1

    assert(words[i].lower() == 'symagg')
    i = i + 1

    agg = words[i]

    if(agg.lower() == 'symcol'):
        agg = None
    else:
        i = i + 1

    assert(words[i].lower() == 'symcol')
    i = i + 1

    sel_col_words = []

    while((words[i].lower() != 'symwhere') & (words[i].lower() != 'symend')):
        sel_col_words.append(words[i])
        i = i + 1


    conds = []
    while(words[i].lower() != 'symend'):
        i = i + 1
        if(words[i].lower() == 'symcol'):
            cond_sel_col_words = []
            i = i + 1
            while(words[i].lower() != 'symop'):
                cond_sel_col_words.append(words[i])
                i = i + 1
            cond_sel_col = ' '.join(cond_sel_col_words)
            
            assert(words[i].lower() == 'symop')
            i = i + 1
            op = words[i]
            i = i + 1
            assert(words[i].lower() == 'symcond')
            i = i + 1
            cond_words = []
            while((words[i].lower() != 'symand') & (words[i].lower() != 'symend')):
                cond_words.append(words[i])
                i = i + 1
            cond_sent = ' '.join(cond_words)
            conds.append((cond_sel_col, op, cond_sent))

    if agg is not None:
        agg = agg.lower()

    sel_col = ' '.join(sel_col_words)

    return (sel_col, agg, conds)

    template = get_template(agg, conds, multi_template, template_list)
    nl = fill_template(sel_col, agg, conds, template, lexical, phrase_dict)
    return nl

def preprocess(sent):
    sent = nltk.word_tokenize(sent)
    sent = nltk.pos_tag(sent)
    return sent

def sql_to_nl(parsed_sql, template_list, phrase_dict, lexical):
    chosen_template = get_template(parsed_sql[1], parsed_sql[2], template_list)
    nl = fill_template(parsed_sql[0], parsed_sql[1], parsed_sql[2], chosen_template, lexical, phrase_dict)
    return nl



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('-input', type=str, default='data/sampled_sql.in')
    parser.add_argument('-output', type=str)
    parser.add_argument('-lexical', action='store_true')
    parser.add_argument('-all_template', action='store_true')
    parser.add_argument('-style', type=str)
    parser.add_argument('-choice', type=str)

    args = parser.parse_args()
    #file_name = 'data/test.in'
    input_file = open(args.input, 'r', encoding='utf-8')
    out_file = open('results/' + args.output, 'w', encoding='utf-8')

    i = 0

    lexical = args.lexical

    style_dict = globals()[args.style]
    template_list = style_dict['templates']

    if args.choice:
        choice = args.choice.split(',')
        tmp = []
        for i in choice:
            tmp.append(template_list[int(i)])
        template_list = tmp
    else:
        print("Using all templates")

    phrase_dict = style_dict['phrases']

    chosen_templates = []
    parsed_sqls = []

    for row in input_file:
        #nl = sql_to_nl(row, lexical, multi_template, template_list, phrase_dict)
        parsed_sql = parse_sql(row.strip())
        nl = sql_to_nl(parsed_sql, template_list, phrase_dict, lexical)
        out_file.write(nl + '\n')

        #chosen_templates.append(get_template(parsed_sql[1], parsed_sql[2], multi_template, template_list))
        #parsed_sqls.append(parsed_sql)

        if debug:
            print([(X.text, X.label_) for X in doc.ents])
            print(a)
            print(nl)
         

        #out_file.write(str(i) + '\t' + nl + '\n')
        #out_file.write(nl + '\n')
        #if i == 20:
            #break
    '''
    assert(len(chosen_templates) == len(parsed_sqls))

    for temp, sql in zip(chosen_templates, parsed_sqls):
        nl = fill_template(sql[0], sql[1], sql[2], temp, lexical, phrase_dict)
        out_file.write(nl + '\n')
    '''
